#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def	test_read_1(self):
        s = "171 171\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  171)
        self.assertEqual(j, 171)

    def test_read_2(self):
        s = "100 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 1000)

    def test_read_3(self):
        s = "37 999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  37)
        self.assertEqual(j, 999)


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(500, 500)
        self.assertEqual(v, 111) 
    
    def test_eval_6(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1) 

    def test_eval_7(self):
        v = collatz_eval(3, 9)
        self.assertEqual(v, 20) 

    def test_eval_8(self):
        v = collatz_eval(206, 207)
        self.assertEqual(v, 89) 

    def test_eval_9(self):
        v = collatz_eval(171, 200)
        self.assertEqual(v, 125)

    def test_eval_10(self):
        v = collatz_eval(200, 171)
        self.assertEqual(v, 125)

    def test_eval_11(self):
        v = collatz_eval(50, 256000)
        self.assertEqual(v, 443)

    def test_eval_12(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_13(self):
        v = collatz_eval(900002, 999999)
        self.assertEqual(v, 507)
    
    def test_eval_14(self):
        v = collatz_eval(151001, 200002)
        self.assertEqual(v, 383)

    def test_eval_15(self):
        v = collatz_eval(19000, 74999)
        self.assertEqual(v, 340)




 
    

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 171, 200, 125)
        self.assertEqual(w.getvalue(), "171 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 999, 1000, 112)
        self.assertEqual(w.getvalue(), "999 1000 112\n")



    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_1(self):
        r = StringIO("204 206\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "204 206 89\n")

    def test_solve_2(self):
        r = StringIO("1 1\n171 200\n500 600\n38 70\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 1 1\n171 200 125\n500 600 137\n38 70 113\n")

    def test_solve_3(self):
        r = StringIO("9 13\n950 850\n300 300\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "9 13 20\n950 850 179\n300 300 17\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
...........................
----------------------------------------------------------------------
Ran 27 tests in 11.055s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
...........................
----------------------------------------------------------------------
Ran 27 tests in 11.055s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          70      0     30      0   100%
TestCollatz.py     107      0      0      0   100%
------------------------------------------------------------
TOTAL              177      0     30      0   100%
"""
